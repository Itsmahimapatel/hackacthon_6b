-- Creating our stocks database 
CREATE DATABASE NASDAQ_Stocks;
USE NASDAQ_Stocks;

-- Creating the tables for various stocks

drop table amazon;
drop table apple;
drop table facebook;
drop table netflix;
drop table microsoft;

create table amazon
(CompanyId int,
CompanyName varchar(4),
trade_date date,
low_value double(15,8),
open_value decimal(15,8),
volume double,
high_value decimal(15,8),
close_value decimal(15,8),
adjustedclose decimal(15,8)
);

create table apple
(CompanyId int,
CompanyName varchar(4),
trade_date date,
low_value double(15,8),
open_value decimal(15,8),
volume double,
high_value decimal(15,8),
close_value decimal(15,8),
adjustedclose decimal(15,8)
);
create table facebook
(CompanyId int,
CompanyName varchar(4),
trade_date date,
low_value double(15,8),
open_value decimal(15,8),
volume double,
high_value decimal(15,8),
close_value decimal(15,8),
adjustedclose decimal(15,8)
);

create table microsoft
(CompanyId int,
CompanyName varchar(4),
trade_date date,
low_value double(15,8),
open_value decimal(15,8),
volume double,
high_value decimal(15,8),
close_value decimal(15,8),
adjustedclose decimal(15,8)
);

create table netflix
(CompanyId int,
CompanyName varchar(4),
trade_date date,
low_value double(15,8),
open_value decimal(15,8),
volume double,
high_value decimal(15,8),
close_value decimal(15,8),
adjustedclose decimal(15,8)
);

-- SHOW GLOBAL VARIABLES LIKE 'local_infile';
-- SET GLOBAL local_infile = 'ON';

LOAD DATA LOCAL INFILE "C:/Users/Administrator/git/hackacthon_6b/src/main/resources/AMZN.csv" INTO TABLE amazon
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
ignore 1 lines;

LOAD DATA LOCAL INFILE "C:/Users/Administrator/git/hackacthon_6b/src/main/resources/AAPL.csv" INTO TABLE apple
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
ignore 1 lines
(CompanyId, CompanyName, @datevar, low_value, open_value, volume, high_value, close_value, adjustedclose)
set trade_date = STR_TO_DATE(@datevar,'%d-%m-%Y');

LOAD DATA LOCAL INFILE "C:/Users/Administrator/git/hackacthon_6b/src/main/resources/MSFT.csv" INTO TABLE microsoft
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
ignore 1 lines
(CompanyId, CompanyName, @datevar, low_value, open_value, volume, high_value, close_value, adjustedclose)
set trade_date = STR_TO_DATE(@datevar,'%d-%m-%Y');

LOAD DATA LOCAL INFILE "C:/Users/Administrator/git/hackacthon_6b/src/main/resources/NFLX.csv" INTO TABLE netflix
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
ignore 1 lines
(CompanyId, CompanyName, @datevar, low_value, open_value, volume, high_value, close_value, adjustedclose)
set trade_date = STR_TO_DATE(@datevar,'%d-%m-%Y');

LOAD DATA LOCAL INFILE "C:/Users/Administrator/git/hackacthon_6b/src/main/resources/FB.csv" INTO TABLE facebook
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
ignore 1 lines;

