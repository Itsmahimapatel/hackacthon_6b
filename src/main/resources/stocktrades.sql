DROP DATABASE IF EXISTS StockTrades;

CREATE DATABASE StockTrades;

USE StockTrades;

CREATE TABLE StockTrades
(id int not null auto_increment, 
clientid varchar(7) not null,
stockTicker varchar(10) not null, 
price double not null,
volume int not null,
buyorsell varchar(4) not null,
statuscode int,
PRIMARY KEY(id));

CREATE TABLE clientInfo
(clientid int not null auto_increment,
clientname varchar(50) not null, 
address varchar(1000) not null,
city varchar(100) not null,
phone varchar(15) not null,
PRIMARY KEY(clientid));

INSERT INTO StockTrades (id, clientid, stockTicker, price, volume, buyorsell, statuscode) VALUES (1,1, 'AAPL',250, 3, 'BUY', 0);
INSERT INTO StockTrades (id, clientid, stockTicker, price, volume, buyorsell, statuscode) VALUES (2,2, 'AMZN',3031, 1, 'BUY', 0);
INSERT INTO StockTrades (id, clientid, stockTicker, price, volume, buyorsell, statuscode) VALUES (3,3, 'MSFT',289, 2, 'BUY', 0);
INSERT INTO StockTrades (id, clientid, stockTicker, price, volume, buyorsell, statuscode) VALUES (4,4,'NFLX',510, 5, 'BUY', 0);
INSERT INTO StockTrades (id, clientid, stockTicker, price, volume, buyorsell, statuscode) VALUES (5,5,'FB',363, 3, 'BUY', 0);
INSERT INTO StockTrades (id, clientid, stockTicker, price, volume, buyorsell, statuscode) VALUES (6,6,'AAPL',260, 2, 'BUY', 0);
INSERT INTO StockTrades (id, clientid, stockTicker, price, volume, buyorsell, statuscode) VALUES (7,5,'AMZN',3030, 3, 'BUY', 0);
INSERT INTO StockTrades (id, clientid, stockTicker, price, volume, buyorsell, statuscode) VALUES (8,2,'FB',370, 3, 'BUY', 0);
INSERT INTO StockTrades (id, clientid, stockTicker, price, volume, buyorsell, statuscode) VALUES (9,4,'NFLX',520, 3, 'BUY', 0);

insert into clientinfo (clientid, clientname, address, city, phone) values (1,'JS','Obere Str. 57', 'Berlin', '(5) 555-3932');
insert into clientinfo (clientid, clientname, address, city, phone) values (2,'MP','120 Hanover Sq.', 'London', '(171) 555-0297');
insert into clientinfo (clientid, clientname, address, city, phone) values (3,'RJ','Berguvsvagen  8', 'LuleA','(604) 555-4729');
insert into clientinfo (clientid, clientname, address, city, phone) values (4,'LM','24, place Kleber', 'Barcelona','(93) 203 4560');
insert into clientinfo (clientid, clientname, address, city, phone) values (5,'AS','12, Hanover St.', 'Berlin','089-0877310');
insert into clientinfo (clientid, clientname, address, city, phone) values (6,'MC','3012, Queen Victoria St.', 'London','(171) 555-0297');


