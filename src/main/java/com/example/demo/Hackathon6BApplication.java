package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hackathon6BApplication {

	public static void main(String[] args) {
		SpringApplication.run(Hackathon6BApplication.class, args);
	}

}
