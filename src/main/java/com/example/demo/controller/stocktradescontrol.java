package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.stocktrades;
import com.example.demo.service.stocktradesservice;

@RestController
@RequestMapping("api/stocktrades")
public class stocktradescontrol {

	@Autowired
	stocktradesservice service;

	@GetMapping(value = "/")
	public List<stocktrades> getAllstocktrades() {
		return service.getAllstocktrades();
	}
	
	@GetMapping(value = "/{id}")
	public stocktrades getstocktradesById(@PathVariable("id") int id) {
	  return service.getstocktrades(id);
	}

	@PostMapping(value = "/")
	public stocktrades addstocktrades(@RequestBody stocktrades stocktrades) {
		return service.newstocktrades(stocktrades);
	}

	@PutMapping(value = "/")
	public stocktrades editstocktrades(@RequestBody stocktrades stocktrades) {
		return service.savestocktrades(stocktrades);
	}

	@DeleteMapping(value = "/{id}")
	public int deletestocktrades(@PathVariable int id) {
		return service.deletestocktrades(id);
	}
}

