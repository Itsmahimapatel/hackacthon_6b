package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.stocktrades;
import com.example.demo.repository.stocktradesrepository;

@Service
public class stocktradesservice {

	@Autowired
	private stocktradesrepository repository;

	public List<stocktrades> getAllstocktrades() {
		return repository.getAllstocktrades();
	}
	
	public stocktrades getstocktrades(int id) {
		return repository.getstocktradesById(id);
	}

	public stocktrades savestocktrades(stocktrades stocktrades) {
		return repository.editstocktrades(stocktrades);
	}

	public stocktrades newstocktrades(stocktrades stocktrades) {
		return repository.addstocktrades(stocktrades);
	}

	public int deletestocktrades(int id) {
		return repository.deletestocktrades(id);
	}
}

