package com.example.demo.repository;

import java.util.List;
import com.example.demo.entities.stocktrades;

public interface stocktradesrepository {
	public List<stocktrades> getAllstocktrades();

	public stocktrades getstocktradesById(int id);

	public stocktrades editstocktrades(stocktrades stocktrades);

	public int deletestocktrades(int id);

	public stocktrades addstocktrades(stocktrades stocktrades);
}
