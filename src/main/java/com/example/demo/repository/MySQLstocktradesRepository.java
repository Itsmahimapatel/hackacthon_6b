package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.stocktrades;

@Repository
public class MySQLstocktradesRepository implements stocktradesrepository {

	@Autowired
	JdbcTemplate template;

	@Override
	public List<stocktrades> getAllstocktrades() {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM stocktrades";
		return template.query(sql, new stocktradesRowMapper());
	}

	@Override
	public stocktrades getstocktradesById(int id) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM stocktrades WHERE id=?";
		return template.queryForObject(sql, new stocktradesRowMapper(), id);
	}

	@Override
	public stocktrades editstocktrades(stocktrades stocktrades) {
		// TODO Auto-generated method stub
		String sql = "UPDATE stocktrades SET clientid = ?, stockticker = ?, price = ?, volume = ?, buyorsell = ?, statuscode = ?" +
				"WHERE id = ?";
		template.update(sql,stocktrades.getClientid(),stocktrades.getStockticker(),stocktrades.getId(),stocktrades.getPrice(),stocktrades.getVolume(),stocktrades.getBuyorsell(),stocktrades.getStatuscode());
		return stocktrades;
	}

	@Override
	public int deletestocktrades(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM stocktrades WHERE id = ?";
		template.update(sql,id);
		return id;
	}

	@Override
	public stocktrades addstocktrades(stocktrades stocktrades) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO stocktrades(Clientid, stockticker, price, volume, buyorsell, statuscode) " +
				"VALUES(?,?,?,?,?,?)";
		template.update(sql,stocktrades.getClientid(),stocktrades.getStockticker(),stocktrades.getPrice(),stocktrades.getVolume(),stocktrades.getBuyorsell(),stocktrades.getStatuscode());
		return stocktrades;
	}
}

class stocktradesRowMapper implements RowMapper<stocktrades> {

	@Override
	public stocktrades mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new stocktrades(rs.getInt("id"), rs.getInt("Clientid"), rs.getString("Stockticker"),rs.getDouble("Price"),rs.getInt("Volume"),rs.getString("Buyorsell"),rs.getInt("statuscode"));
	}

}
