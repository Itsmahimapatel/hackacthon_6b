package com.example.demo.entities;

public class stocktrades {
	private int id;
	private int clientid;
	private String stockticker;
	private double price;
	private int volume;
	private String buyorsell;
	private int statuscode;

	public stocktrades(int id, int clientid, String stockticker, double price, int volume, String buyorsell, int statuscode) {
		super();
		this.id = id;
		this.clientid=clientid;
		this.stockticker=stockticker;
		this.price=price;
		this.volume=volume;
		this.buyorsell=buyorsell;
		this.statuscode=statuscode;
	}

	public stocktrades() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClientid() {
		return clientid;
	}

	public void setClientid(int clientid) {
		this.clientid = clientid;
	}

	public String getStockticker() {
		return stockticker;
	}

	public void setStockticker(String stockticker) {
		this.stockticker = stockticker;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public String getBuyorsell() {
		return buyorsell;
	}

	public void setBuyorsell(String buyorsell) {
		this.buyorsell = buyorsell;
	}

	public int getStatuscode() {
		return statuscode;
	}

	public void setStatuscode(int statuscode) {
		this.statuscode = statuscode;
	}
}